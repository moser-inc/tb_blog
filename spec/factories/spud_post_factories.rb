FactoryBot.define do

  sequence :spud_post_title do |n|
    "Test Blog Post #{n}"
  end

  factory :spud_post do
    author { FactoryBot.create(:spud_user) }
    title { FactoryBot.generate(:spud_post_title) }
    content 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    comments_enabled true
    visible true
    published_at DateTime.now
    content_format 'HTML'
    blog_key 'blog'
  end

end
