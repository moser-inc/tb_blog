require 'rails_helper'

RSpec.describe BlogHelper, type: :helper do

  describe '#disqus_comment_count_for_post' do
    it 'should return a tag with the disqus identifier' do
      post = FactoryBot.create(:spud_post)
      result = helper.disqus_comment_count_for_post(post)
      expect(result).to include("data-disqus-identifier=\"#{post.identifier}\"")
    end
  end

end
