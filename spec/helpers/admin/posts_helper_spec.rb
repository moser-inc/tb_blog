require 'rails_helper'

RSpec.describe Admin::PostsHelper, type: :helper do

  describe '#link_to_disqus_comment_count' do
    it 'should return a link with the disqus identifier' do
      post = FactoryBot.create(:spud_post)
      result = helper.link_to_disqus_comment_count(post)

      expect(result).to include("data-disqus-identifier=\"#{post.identifier}\"")
      expect(result).to include('testblog.disqus.com')
    end
  end

end
