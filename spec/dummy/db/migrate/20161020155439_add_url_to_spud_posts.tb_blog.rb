# This migration comes from tb_blog (originally 20120127143054)
class AddUrlToSpudPosts < ActiveRecord::Migration
  def change
    add_column :spud_posts, :url_name, :string
    add_index :spud_posts, :url_name
  end
end
