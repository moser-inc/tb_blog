# This migration comes from tb_blog (originally 20151015151133)
class AddCustomAuthorToPost < ActiveRecord::Migration
  def change
    add_column :spud_posts, :custom_author, :string
  end
end
