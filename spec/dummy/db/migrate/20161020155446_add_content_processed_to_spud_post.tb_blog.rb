# This migration comes from tb_blog (originally 20130121130612)
class AddContentProcessedToSpudPost < ActiveRecord::Migration
  def change
    add_column :spud_posts, :content_processed, :text
  end
end
