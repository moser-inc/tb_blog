# This migration comes from tb_blog (originally 20121113135812)
class AddNestedSetToPostCategories < ActiveRecord::Migration
  def up
    change_table :spud_post_categories do |t|
      t.integer :lft
      t.integer :rgt
      t.integer :depth
    end
  end

  def down
    change_table :spud_post_categories do |t|
      t.remove :lft
      t.remove :rgt
      t.remove :depth
    end
  end
end
