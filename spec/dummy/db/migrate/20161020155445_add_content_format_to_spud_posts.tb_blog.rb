# This migration comes from tb_blog (originally 20130120151857)
class AddContentFormatToSpudPosts < ActiveRecord::Migration
  def change
    add_column :spud_posts, :content_format, :string, default: 'HTML'
  end
end
