# This migration comes from tb_blog (originally 20160216144708)
class AddIdentifierToSpudPosts < ActiveRecord::Migration
  def change
    add_column :spud_posts, :identifier, :string
    add_index :spud_posts, :identifier, unique: true
  end
end
