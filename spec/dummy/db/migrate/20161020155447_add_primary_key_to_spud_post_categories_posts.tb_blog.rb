# This migration comes from tb_blog (originally 20140406182651)
class AddPrimaryKeyToSpudPostCategoriesPosts < ActiveRecord::Migration
  def change
    add_column :spud_post_categories_posts, :id, :primary_key
  end
end
