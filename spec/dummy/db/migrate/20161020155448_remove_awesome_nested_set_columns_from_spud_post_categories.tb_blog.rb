# This migration comes from tb_blog (originally 20140508200730)
class RemoveAwesomeNestedSetColumnsFromSpudPostCategories < ActiveRecord::Migration
  def up
    change_table :spud_post_categories do |t|
      t.remove :lft, :rgt, :depth, :parent_id
    end
  end

  def down
    change_table :spud_post_categories do |t|
      t.integer :lft
      t.integer :rgt
      t.integer :depth
      t.integer :parent_id
    end
  end
end
