# This migration comes from tb_blog (originally 20120309181917)
class AddMetaToPosts < ActiveRecord::Migration
  def change
    add_column :spud_posts, :meta_keywords, :string
    add_column :spud_posts, :meta_description, :text
  end
end
