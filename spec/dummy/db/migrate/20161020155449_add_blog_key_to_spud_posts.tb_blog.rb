# This migration comes from tb_blog (originally 20140808132950)
class AddBlogKeyToSpudPosts < ActiveRecord::Migration
  def change
    add_column :spud_posts, :blog_key, :string
    add_index :spud_posts, :blog_key
  end
end
