require 'rails_helper'

RSpec.describe SpudPost, type: :model do

  describe '#display_date' do
    it 'should return a date string' do
      post = FactoryBot.create(:spud_post)
      expect(post.display_date).to eq(post.published_at.strftime('%b %d, %Y'))
    end
  end

  describe '#set_identifier' do
    it 'should set a random identifier' do
      post = FactoryBot.create(:spud_post, identifier: nil)
      expect(post.identifier).to be_a(String)
    end
  end

end
