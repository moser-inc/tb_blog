require 'rails_helper'

describe Admin::PostsController, type: :controller do

  before(:each) do
    activate_session(admin: true)
  end

  describe '#create_redirect_if_necessary' do
    before(:each) do
      @post = FactoryBot.create(:spud_post)
    end

    it 'should create a redirect' do
      expect do
        put :update, params: { id: @post.id, spud_post: { url_name: 'new-url' }, blog_key: 'blog' }
      end.to change(@post.tb_redirects, :count).by(1)
    end

    it 'should not create a redirect' do
      expect do
        put :update, params: { id: @post.id, spud_post: { title: 'Hello World' }, blog_key: 'blog' }
      end.to_not change(@post.tb_redirects, :count)
    end
  end

  describe '#preview' do
    context 'with a new post' do
      it 'should set the attributes without saving' do
        expect do
          post :preview, params: { blog_key: 'blog', post_id: nil, spud_post: { title: 'Hello World' } }
        end.to_not change(SpudPost, :count)
      end
    end
    context 'with an existing post' do
      it 'should set the attributes without saving' do
        @post = FactoryBot.create(:spud_post)
        expect do
          post :preview, params: { blog_key: @post.blog_key, post_id: @post.id, spud_post: { title: 'Hello World' } }
          @post.reload
        end.to_not change(@post, :title)
      end
    end
  end
end
