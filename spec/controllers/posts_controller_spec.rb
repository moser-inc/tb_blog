require 'rails_helper'

describe PostsController, type: :controller do

  before(:each) do
    activate_authlogic
  end

  describe 'index' do
    it 'should display a list of posts' do
      2.times { |_i| FactoryBot.create(:spud_post) }
      get :index, params: { blog_key: 'blog' }
      expect(assigns(:posts).count).to be > 1
    end

    it 'should not display any posts' do
      Spud::Blog.config.query_for_user = ->(user) {
        if user.blank?
          '1=0'
        else
          {}
        end
      }
      get :index, params: { blog_key: 'blog' }
      expect(assigns(:posts).count).to be 0
      Spud::Blog.config.query_for_user = nil
    end
  end

  describe 'show' do
    it 'renders the post' do
      post = FactoryBot.create(:spud_post)
      get :show, params: { blog_key: post.blog_key, id: post.url_name }
      expect(response).to have_http_status(:success)
    end
  end

end
