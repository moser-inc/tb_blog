# This migration will drop spud_post_comments from your database. It has been excluded
# from the migrations directory to avoid accidential data loss on upgrading.
#
# If you wish to completely remove spud_post_comments from your app, simply copy this file
# into your db/migrations directory and run db:migrate.
# 

class DropSpudPostComments < ActiveRecord::Migration
  def up
    drop_table :spud_post_comments
    remove_column :spud_posts, :comments_count
  end
  def down
    create_table :spud_post_comments do |t|
      t.integer :spud_post_id
      t.string :author
      t.text :content
      t.boolean :approved, :default => false
      t.boolean :spam
      t.string :user_ip
      t.string :user_agent
      t.string :referrer
      t.string :permalink
      t.string :blog_key
      t.timestamps
    end
    add_column :spud_posts, :comments_count, :integer
  end
end
