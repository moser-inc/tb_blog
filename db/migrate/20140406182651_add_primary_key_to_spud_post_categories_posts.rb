class AddPrimaryKeyToSpudPostCategoriesPosts < ActiveRecord::Migration
  def change
    add_column :spud_post_categories_posts, :id, :primary_key
  end
end
