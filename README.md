TB Blog
========

TB Blog is a Blog Engine designed to be robust, easy to use, and light weight.

## Installation/Usage

1. In your Gemfile add the following

		gem 'tb_core'
		gem 'tb_blog'

2. Run bundle install
3. Copy in database migrations to your new rails project

		rails g spud:setup

4. run a rails server instance and point your browser to /admin

## Configuration

TB Blog current accepts the following configuration options.

	Spud::Blog.configure do |config|
	  config.base_layout = 'blog'
	  config.blog_enabled = true
	  config.news_enabled = true
	  config.blog_path = 'blog'
	  config.news_path = 'news'
	  config.posts_per_page = 5
    config.disqus_shortname = ''
	end

## Customizing Views

A number of built-in views have been provided to help you get started with the frontend display. Customzing these views will require you to copy them into your local application, which can be accomplished by using the views generator.

	rails generate spud:blog:views

__NOTE:__ The built-in views are likely to undergo changes as features are added to the blogging engine. If a new version of TB Blog does not play nicely with your customized views, try backing up your views to an alternate location and running the views generator again to see what has changed.

## Custom Blogs

TB Blog has the ability to define arbitrary blogs outside of the typical Blog and News feeds. To get a custom blog up and running quickly, simply run the provided generator.

    rails g spud:blog:blog "Press Releases"

The generator will have inserted the following snippet of code into your `application.rb` file:

    Spud::Blog.config.blogs << {
      :name => 'Press Releases',        # Display name for the blog
      :key => 'press-releases',         # Used by the controller and database to key each post
      :path => '/about/press-releases', # Front-end url for the blog
      :layout => 'about'                # Which erb layout should be used
    }

By default, custom blogs will use the standard `index` and `show` views under `app/views/posts`. However, you may choose to customize those views by copying the same files over to `app/views/[BLOG KEY]`. Files in that directory will take precedence over those in the standard directory.

### Upgrading from v1.1.2 or Lower

The new blog configuration system required a change to the way we determine blog vs news (vs other) posts. After upgrading your gems and running the migrations, you should run the following rake task to update your data model:

    rake tb_blog:apply_blog_keys

The `is_news` column is being left on the `SpudPost` model for the time being to facilitate this transation. At some point in a future version will we remove that column entirely. Please make sure to transition your data before the column goes away for good.

## Access Control

TB Blog offers a mechanism for limitting who can see what posts. Supply a lambda to the `query_for_user` config, which takes a single `user` argument. The value you return will be supplied directly to the `where()` query used to find posts.

The example below is from an application that allows subscribers to view subscriber-only content.

	Spud::Blog.query_for_user = ->(user){
    	if !user.try(:has_paid_subscription?)
      		{:requires_subscription => false}
    	end
	}

*See the section on Custom Fields to see how you might go about adding such a field to your model.*

## Custom Fields

You may find that your blog requires a field that isn't included in the default `spud_post` model. Adding custom fields is easy.

1. Create a migration adding the necessary column(s) to your database

		class AddCaptionToPosts < ActiveRecord::Migration
		  def change
		    add_column :spud_posts, :requires_subscription, :boolean, :default => false
		  end
		end

2. Save a view partial at `app/views/admin/posts/_custom_fields.html.erb` with the desired inputs

		<h4>My Custom Fields</h4>
		<div class="spud_post_form_row">
		  <%= f.label :requires_subscription, 'Require subscription' %>
		  <%= f.check_box :requires_subscription %>
		</div>

3. Finally, add the field to your `permitted_attributes` array. This is to satisfy the [Strong Parameters](http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters) in the admin controller.

		Spud::Blog.configure do |config|
			config.permitted_attributes = [:requires_subscription]
		end

## Extending the Post Model

You might want to add custom methods or attributes to the Post model. Create a file at `app/models/spud_post.rb` and add the following code:

	class SpudPost < Spud::SpudPostModel
		# custom methods here!
		def hello_world
			return "Hello, World!"
		end
	end

That's it! You may now begin customizing the post class.

	p = SpudPost.first
	puts p.hello_world # outputs "Hello, World!"

Testing
-----------------

Spud uses RSpec for testing. Get the tests running with a few short commands:

1. Create and migrate the databases:

        rake db:create
        rake db:migrate

2. Load the schema in to the test database:

        rake app:db:test:prepare

3. Run the tests with RSpec

        rspec spec

After the tests have completed the current code coverage stats is available by opening ```/coverage/index.html``` in a browser.
