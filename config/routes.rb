Rails.application.routes.draw do

  namespace :admin do
    resources :post_categories
  end

  SpudBlogConfig.each do |config|
    blog_key = config.key

    namespace :admin do
      resources :posts, :path => blog_key, :blog_key => blog_key do
        match :preview, on: :collection, via: [:post, :patch]
      end
    end

    scope config.path do

      # Blog Post Categories
      get 'category/:category_url_name(/page/:page)' => 'posts#category',
        :as => "#{blog_key}_category",
        :blog_key => blog_key,
        :defaults => {:page => 1}

      # Blog Post Archives
      get 'archive/:archive_date(/page/:page)' => 'posts#archive',
        :as => "#{blog_key}_archive",
        :blog_key => blog_key,
        :defaults => {:page => 1}

      # Blog Post Category + Archive
      get 'category/:category_url_name/:archive_date(/page/:page)',
        :controller => 'posts',
        :action => 'category',
        :as => "#{blog_key}_category_archive",
        :blog_key => blog_key,
        :defaults => {:page => 1}

      # Category/Archive filtering
      post '/', :controller => 'posts', :action => 'filter', :blog_key => blog_key

      # Blog Posts
      get '/(page/:page)' => 'posts#index',
        :as => "#{blog_key}_posts",
        :blog_key => blog_key,
        :defaults => {:page => 1}

      get ':id' => 'posts#show', :as => "#{blog_key}_post", :blog_key => blog_key
    end
  end

end
