# Change Log

## v1.4.0

- Add support for Rails 5
- Remove rarely used liquid template features

## v1.3.4

- Add a preview function in the admin

## v1.3.3

- Removes the comments model entirely in favor of Disqus integration
- Replace tb_permalinks with tb_redirects

## v1.3.2

- You can now specify your own url slug when creating a blog post
- Front end views are not bootstrap-friendly by default

## v1.3.0

- Compatability with Rails 4.2, TB Core 1.3, and Bootstrap 3.

## v1.2.0

This is a big update, as we now support the notion of defining your own custom blogs outside of the typical Blog and News use cases! See the readme for details.

## v1.1.2

- This version removes nested categories, along with the `awesome_nested_set` dependency. In practice this caused much complexity and was infrequently used. Expect to see blog moving to a more tagging-like system in the future.
- Adds `Spud::SpudPostModel` model class for apps to extend. See Readme for details.
- Deprecates many class methods in `SpudPost` and replaces them with scopes
- Bug fixes

## v1.1.1

- Show number of pending comments in blog badge counter

## v1.1.0

- Support for Rails 4 and tb_core 1.2
- Fragment caching for blog and news pages

## v1.0.4

- Fixes bug where `approved` was ignored in `SpudPostComment`
- New and improved UI for post comment management
- Added `default_comment_approval` configuration option
