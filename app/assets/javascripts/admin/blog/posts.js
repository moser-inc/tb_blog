(function(){

spud.admin.posts = {
  edit: function(){
    $('body').on('click', '.spud-post-add-category', clickedPostAddCategory);
    $('body').on('submit', '.spud-post-category-form', submittedPostCategoryForm);
    $('body').on('blur', '.spud-post-title', blurredPostTitle);
    $('body').on('click', '.spud-post-url-name-regen', clickedUrlNameRegen);
    $(".btn-preview").on('click', clickedPreviewButton);
  }
};

var clickedPostAddCategory = function(e){
  e.preventDefault();
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      spud.admin.modal.displayWithOptions({
        title: 'Add Category',
        html: html
      });
    }
  });
};

var submittedPostCategoryForm = function(e){
  e.preventDefault();
  var form = $('.spud-post-category-form');
  $.ajax({
    url: form.attr('action'),
    data: form.serialize(),
    type: 'post',
    dataType: 'json',
    success: savedPostCategorySuccess,
    error: savePostCategoryError
  });
};

var savedPostCategorySuccess = function(data, textStatus, jqXHR){
  var checkbox = '';
  checkbox += '<li class="spud_post_form_category" data-id="'+data.id+'">';
  checkbox += '<input id="spud_post_category_'+data.id+'" name="spud_post[category_ids][]" type="checkbox" value="'+data.id+'" checked>' + "\n";
  checkbox += '<label for="spud_post_category_'+data.id+'">'+data.name+'</label>';
  checkbox += '<ul></ul></li>';
  $('.spud-post-categories-list').append(checkbox).scrollTop(99999);
  spud.admin.modal.hide();
};

var savePostCategoryError = function(jqXHR, textStatus, errorThrown){
  if(jqXHR.status == 422){
    var html = jqXHR.responseText;
    $('.spud-post-category-form').replaceWith(html);
  }
  else{
    if(window.console){
      console.error('Oh Snap:', arguments);
    }
  }
};

var blurredPostTitle = function(){
  generateUrlName(false);
};

var clickedUrlNameRegen = function(e){
  e.preventDefault();
  generateUrlName(true);
};

/*
* Populate the url name field
*
* Pass force=true to replace the value currently in the field
*/
var generateUrlName = function(force){
  var title = $('.spud-post-title').val();
  var $urlNameField = $('.spud-post-url-name');
  if(title !== '' && (force || $urlNameField.val() === '')){
    $urlNameField.val(encodeTitleForUrlName(title));
  }
};

/*
* Return a url name for a given title
*/
var encodeTitleForUrlName = function(title){
  var string;
  string = title.toLowerCase();
  string = string.replace(/\s+/g, '-'); // replace spaces with dashes
  string = string.replace(/[^a-z0-9\-]+/g, ''); // remove symbols
  return string;
};

var clickedPreviewButton = function(e){
  e.preventDefault();
  var $button = $(this);
  var $form = $button.parents('form');

  // Build a temporary form
  $previewForm = $('<form />', {
    action: $form.data('previewAction'),
    target: '_blank',
    method: 'POST',
    css: { display: 'none' }
  });
  $('body').append($previewForm);

  // Tell tinymce to write out to the textarea
  tinyMCE.triggerSave();

  // Copy fields into our temp form
  $form.find('input, select, textarea').each(function(i, el){
    var input = document.createElement('input');
    input.name = el.name;
    input.value = el.value;
    $previewForm.append(input);
  });

  // Cross site scripting tag
  var csrf = document.createElement('input');
  csrf.name = 'authenticity_token';
  csrf.value = Rails.csrfToken();
  $previewForm.append(csrf);

  // Submit it
  $button.button('loading');
  $previewForm.submit();

  setTimeout(function(){
    $previewForm.remove();
    $button.button('reset');
  }, 2000);
};

})();
