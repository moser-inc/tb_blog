(function(){

spud.admin.post_categories = {
  init: function(){
    $('body').on('click', '.spud-blog-manage-categories', clickedManageCategories);
    $('body').on('click', '.spud-blog-category-add-new', clickedAddNewCategory);
    $('body').on('click', '.spud-blog-category-edit', clickedEditCategory);
    $('body').on('click', '.spud-blog-manage-categories-back', clickedBackButton);
    $('body').on('click', '.spud-blog-manage-categories-save', submittedPostCategoryForm);
    $('body').on('submit', '.spud-post-category-form', submittedPostCategoryForm);
    $('body').on('click', '.spud-blog-category-delete', clickedDeleteCategory);
  }
};

var _cachedCategoryIndex;

var clickedManageCategories = function(e){
  e.preventDefault();
  $.ajax({
    url: $(this).attr('href'),
    success: function(html, textStatus, jqXHR){
      _cachedCategoryIndex = html;
      spud.admin.modal.displayWithOptions({
        title: 'Manage Categories',
        html: html,
        buttons: {
          'spud-blog-manage-categories-back': 'Back',
          'spud-blog-manage-categories-save btn-primary': 'Save'
        }
      });
    }
  });
};

var clickedAddNewCategory = function(e){
  e.preventDefault();
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      $('.modal-footer button[data-dismiss="modal"]').hide();
      $('.spud-blog-manage-categories-save, .spud-blog-manage-categories-back').show();
      $('.spud-blog-category-manager').replaceWith(html);
    }
  });
};

var clickedEditCategory = function(e){
  e.preventDefault();
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      $('.modal-footer button[data-dismiss="modal"]').hide();
      $('.spud-blog-manage-categories-save, .spud-blog-manage-categories-back').show();
      $('.spud-blog-category-manager').replaceWith(html);
    }
  });
};

var clickedBackButton = function(e){
  e.preventDefault();
  $('.spud-post-category-form').replaceWith(_cachedCategoryIndex);
  $('.modal-footer button[data-dismiss="modal"]').show();
  $('.spud-blog-manage-categories-save, .spud-blog-manage-categories-back').hide();
};

var clickedDeleteCategory = function(e){
  e.preventDefault();
  if(window.confirm('Are you sure you want to delete this category?')){
    $.ajax({
      url: $(this).attr('href'),
      data: {_method: 'delete'},
      type: 'delete',
      dataType: 'html',
      success: function(html, textStatus, jqXHR){
        _cachedCategoryIndex = html;
        $('.spud-blog-category-manager').replaceWith(html);
      }
    });
  }
};

var submittedPostCategoryForm = function(e){
  e.preventDefault();
  var form = $('.spud-post-category-form');
  $.ajax({
    url: form.attr('action'),
    data: form.serialize(),
    type: 'post',
    dataType: 'html',
    success: savedPostCategorySuccess,
    error: savePostCategoryError
  });
};

var savedPostCategorySuccess = function(html, textStatus, jqXHR){
  _cachedCategoryIndex = html;
  $('.spud-blog-manage-categories-save, .spud-blog-manage-categories-back').hide();
  $('.spud-post-category-form').replaceWith(html);
};

var savePostCategoryError = function(jqXHR, textStatus, errorThrown){
  if(jqXHR.status == 422){
    var html = jqXHR.responseText;
    $('.spud-post-category-form').replaceWith(html);
  }
  else{
    if(window.console){
      console.error('Oh Snap:', arguments);
    }
  }
};

})();
