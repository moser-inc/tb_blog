module BlogHelper

  def options_for_post_category_select
    return options_from_collection_for_select(SpudPostCategory.ordered, :url_name, :name, params[:category_url_name])
  end

  def options_for_post_archive_select
    return options_for_select(SpudPost.months_with_public_posts.collect do |d|
      [d.strftime('%B %Y'), d.strftime('%Y-%b').downcase]
    end, params[:archive_date])
  end

  def spud_post_category_select
    return select_tag('category_url_name', options_for_post_category_select(), prompt: 'All Categories', class: 'form-control')
  end

  def spud_post_archive_select
    return select_tag 'archive_date', options_for_post_archive_select(), prompt: 'All Dates', rel: 'archive', class: 'form-control'
  end

  def spud_blog_rss_link
    return tag :link, rel: 'alternate', type: 'application/rss+xml', title: "#{TbCore.site_name} Blog RSS", href: posts_path(format: :rss)
  end

  def disqus_comment_count_for_post(post)
    return content_tag :span, 'Comments', class: 'disqus-comment-count',
                                          data: { disqus_identifier: post.identifier }
  end

end
