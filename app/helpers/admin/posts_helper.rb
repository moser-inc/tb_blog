module Admin::PostsHelper

  def options_for_parent_category(parent_id = 0)
    if @categories[parent_id]
      return @categories[parent_id].collect do |c|
        opts = [c.name, c.id]
        opts += options_for_parent_category(c.id)
      end
    else
      return []
    end
  end

  def link_to_disqus_comment_count(post)
    # TODO: It would be great to link directly to the thread
    #   ie: https://myblog.disqus.com/admin/moderate/#/all/search/thread:4584301535
    #
    # However, we do not know the thread ID at this time. We may be able to fetch it
    # via the Disqus JavaScript API.
    #
    return link_to 'Comments', "https://#{Spud::Blog.disqus_shortname}.disqus.com/admin/moderate/#/all", target: :blank, class: 'disqus-comment-count',
                                                                                                         data: { disqus_identifier: post.identifier }
  end

end
