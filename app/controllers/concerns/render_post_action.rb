module RenderPostAction
  extend ActiveSupport::Concern

  def render_post_action(action, args = {})
    render action, args.merge(prefixes: [@config.key, 'posts'])
  end
end
