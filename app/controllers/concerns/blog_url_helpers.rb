module BlogUrlHelpers
  extend ActiveSupport::Concern

  included do
    helper_method :posts_path, :post_path, :post_category_path, :post_archive_path,
      :admin_posts_path, :admin_post_path, :new_admin_post_path, :edit_admin_post_path,
      :admin_post_preview_path
  end

  def posts_path(opts = {})
    url_for opts.merge(controller: '/posts', action: 'index', blog_key: params[:blog_key])
  end

  def post_path(url_name, opts = {})
    url_for opts.merge(controller: '/posts', action: 'show', id: url_name, blog_key: params[:blog_key])
  end

  def post_category_path(category_url_name, opts = {})
    url_for opts.merge(controller: '/posts', action: 'category', category_url_name: category_url_name, blog_key: params[:blog_key])
  end

  def post_archive_path(archive_date, opts = {})
    url_for opts.merge(controller: '/posts', action: 'archive', archive_date: archive_date, blog_key: params[:blog_key])
  end

  def post_category_archive_path(category_url_name, archive_date, opts = {})
    url_for opts.merge(controller: '/posts', action: 'category', category_url_name: category_url_name, archive_date: archive_date, blog_key: params[:blog_key])
  end

  #
  # Admin
  #

  def admin_posts_path(blog_key: nil)
    blog_key ||= params[:blog_key]
    url_for controller: '/admin/posts', action: 'index', blog_key: blog_key
  end

  def new_admin_post_path(blog_key: nil)
    blog_key ||= params[:blog_key]
    url_for controller: '/admin/posts', action: 'new', blog_key: blog_key
  end

  def admin_post_path(post)
    url_for controller: '/admin/posts', action: 'show', id: post.id, blog_key: post.blog_key
  end

  def edit_admin_post_path(post)
    url_for controller: '/admin/posts', action: 'edit', id: post.id, blog_key: post.blog_key
  end

  def admin_post_preview_path(post)
    url_for controller: '/admin/posts', action: :preview, post_id: post.id, blog_key: (post.blog_key || params[:blog_key])
  end
end
