class PostsController < ApplicationController
  include BlogUrlHelpers
  include RenderPostAction

  respond_to :html, :json, :rss

  before_action :load_blog
  before_action :load_page_number, only: [:index, :archive, :category]
  before_action :find_post, only: [:show, :create_comment]

  def index
    @posts = SpudPost.for_user(current_user).visible.for_blog(params[:blog_key]).ordered.paginate(page: @page_number, per_page: Spud::Blog.config.posts_per_page)
    @posts = @posts.search(params[:search]) if params[:search]
    respond_with @posts do |format|
      format.html do
        render_post_action :index
      end
    end
  end

  # The sole purpose of this action is to redirect from a POST to an seo-friendly url
  def filter
    if params[:category_url_name].present? && params[:archive_date].present?
      redirect_to post_category_archive_path(params[:category_url_name], params[:archive_date])
    elsif params[:category_url_name].present?
      redirect_to post_category_path(params[:category_url_name])
    elsif params[:archive_date].present?
      redirect_to post_archive_path(params[:archive_date])
    else
      redirect_to posts_path
    end
  end

  def category
    if @post_category = SpudPostCategory.find_by(url_name: params[:category_url_name])
      @posts = @post_category.posts.for_user(current_user).visible.for_blog(params[:blog_key]).from_archive(params[:archive_date]).ordered.paginate(page: @page_number, per_page: Spud::Blog.config.posts_per_page)
    else
      redirect_to posts_path
      return
    end
    respond_with @posts do |format|
      format.html { render_post_action :index }
    end
  end

  def archive
    @posts = SpudPost.for_user(current_user).visible.for_blog(params[:blog_key]).from_archive(params[:archive_date]).paginate(page: @page_number, per_page: Spud::Blog.config.posts_per_page)
    respond_with @posts do |format|
      format.html { render_post_action :index }
    end
  end

  def show
    respond_with @post do |format|
      format.html { render_post_action :show }
    end
  end

  private

  def find_post
    @post = SpudPost.for_user(current_user).for_blog(params[:blog_key]).where(url_name: params[:id]).first
    raise TbCore::NotFoundError, item: 'post' if @post.blank? || @post.is_private?
  end

  def load_page_number
    page = 1
    if params[:page].present?
      page = params[:page].to_i
      if page.to_s != params[:page].to_s
        if page > 1
          redirect_to(posts_path(page: page), status: :moved_permanently) && return
        else
          redirect_to(posts_path(page: nil), status: :moved_permanently) && return
        end
      end
    end
    @page_number = page
  end

  def load_blog
    @config = SpudBlogConfig.find(params[:blog_key])
    if @config.blank?
      raise TbCore::NotFoundError(item: 'blog')
    else
      self.class.layout(@config.layout)
    end
  end

end
