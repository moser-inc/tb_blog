class Admin::PostCategoriesController < Admin::ApplicationController

  layout false
  respond_to :html, :json

  before_action :find_category, only: [:show, :edit, :update, :destroy]

  def index
    @post_categories = SpudPostCategory.ordered
    respond_with @post_categories
  end

  def edit
    respond_with @post_category
  end

  def update
    if @post_category.update(category_params)
      flash.now[:notice] = 'Post Category was successfully updated'
      respond_with @post_category, location: admin_post_categories_path
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def new
    @post_category = SpudPostCategory.new
    respond_with @post_category
  end

  def create
    @post_category = SpudPostCategory.new(category_params)
    if @post_category.save
      flash.now[:notice] = 'Post Category was successfully created'
      respond_with @post_category, location: admin_post_categories_path
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def destroy
    if @post_category.destroy
      flash.now[:notice] = 'Post Category was successfully deleted'
      @post_categories = SpudPostCategory.ordered
      render 'index'
    else
      respond_with @post_category, location: admin_post_categories_path
    end
  end

  private

  def find_category
    @post_category = SpudPostCategory.find(params[:id])
  end

  def category_params
    params.require(:spud_post_category).permit(:name)
  end

end
