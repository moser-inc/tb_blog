class SpudPostCategory < ActiveRecord::Base

  has_many :spud_post_categories_posts,
    foreign_key: :spud_post_category_id,
    inverse_of: :spud_post_category
  has_many :posts,
    class_name: 'SpudPost',
    through: :spud_post_categories_posts,
    source: :spud_post,
    inverse_of: :categories

  validates :name, :url_name, presence: true
  validates :name, :url_name, uniqueness: true
  before_validation :set_url_name

  after_update :touch_posts
  after_destroy :touch_posts

  scope :ordered, -> { order('name asc') }

  private

  def set_url_name
    self.url_name = name.parameterize
  end

  def touch_posts
    posts.update_all(updated_at: Time.now) if name_changed?
  end

end
