class TbCore::SpudPostModel < ActiveRecord::Base
  include TbRedirects::HasRedirects

  self.table_name = 'spud_posts'

  has_many :spud_post_categories_posts,
    foreign_key: :spud_post_id,
    inverse_of: :spud_post
  has_many :categories,
    class_name: 'SpudPostCategory',
    through: :spud_post_categories_posts,
    source: :spud_post_category,
    inverse_of: :posts

  belongs_to :author, class_name: 'SpudUser', foreign_key: 'spud_user_id'

  scope :blog_posts, -> { for_blog('blog') }
  scope :news_posts, -> { for_blog('news') }
  scope :for_blog, ->(key) { where(blog_key: key) }
  scope :visible, -> { where('visible = true AND published_at <= ?', Time.now.utc) }
  scope :ordered, -> { order('published_at desc') }

  scope :search, ->(term) { where('title LIKE ?', "%#{term}%") }
  scope :for_user, ->(user) {
    if Spud::Blog.query_for_user.present?
      where(Spud::Blog.query_for_user.call(user))
    else
      where({})
    end
  }

  validates :title, :content, :published_at, :spud_user_id, presence: true
  validates :url_name, presence: true, uniqueness: true, format: { with: /\A[\w\-]+\z/, message: 'must contain only letters, numbers, and dashes' }
  validates :url_name, uniqueness: true
  before_validation :set_url_name
  before_validation :set_identifier

  def self.for_spud_site(_spud_site_id)
    ActiveSupport::Deprecation.warn 'SpudBlog.for_spud_site is deprecated and will be removed in the future'
    return all()
  end

  def self.recent_posts(limit = 5)
    return where('visible = ? AND published_at <= ?', true, Time.now.utc).order('published_at desc').limit(limit)
  end

  def self.recent_blog_posts(limit = 5)
    return blog_posts.recent_posts(limit)
  end

  def self.recent_news_posts(limit = 5)
    return news_posts.recent_posts(limit)
  end

  def self.from_archive(date_string)
    date = Date.strptime(date_string, '%Y-%b')
    return where(published_at: date..date.end_of_month)
  rescue StandardError
    logger.debug 'fallback'
    return where('')
  end

  def is_news
    ActiveSupport::Deprecation.warn ':is_news is deprecated. Please rely on the :blog_key column stead.', caller
    return self[:is_news]
  end

  def self.months_with_public_posts
    records = SpudPost.select('Extract(Month from published_at) as published_month, Extract(Year from published_at) as published_year').where('visible = ? AND published_at < ?', true, DateTime.now).group('published_month, published_year').order('published_year desc, published_month desc')
    begin
      return records.collect { |r| Date.new(r[:published_year].to_i, r[:published_month].to_i) }
    rescue Exception => e
      logger.fatal "Exception occurred while fetching post archive dates:\n #{e}"
      return []
    end
  end

  # def postprocess_content
  #   template = Liquid::Template.parse(content)
  #   self.content_processed = template.render()
  # end
  #
  # def content_processed
  #   postprocess_content if self[:content_processed].blank?
  #   self[:content_processed]
  # end

  # def content_processed=(content)
  #   self[:content_processed] = content
  # end

  def content_processed
    ActiveSupport::Deprecation.warn '#content_processed is deprecated. Use #content instead.'
    content
  end

  def display_date
    return published_at.strftime('%b %d, %Y')
  end

  def is_public?
    return (published_at < DateTime.now) && visible
  end

  def is_private?
    return !is_public?
  end

  def category_names
    return categories.collect(&:name).join(', ')
  end

  def author_name
    if custom_author.present?
      custom_author
    elsif author.present?
      author.full_name
    else
      ''
    end
  end

  private

  def set_url_name
    self.url_name = title.parameterize if url_name.blank?
  end

  def set_identifier
    self.identifier = SecureRandom.uuid if identifier.blank?
    return true
  end

end
