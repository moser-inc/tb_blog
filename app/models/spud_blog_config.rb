class SpudBlogConfig

  def self.push(config)
    Spud::Blog.config.blogs.push(new(config))
  end

  def self.find(key)
    return Spud::Blog.config.blogs.find { |it| it.key == key }
  end

  def self.each
    Spud::Blog.config.blogs.each do |config|
      yield(config)
    end
  end

  attr_accessor :name, :key, :path, :layout

  def initialize(name:, key:, path:, layout: nil)
    @name = name
    @key = key
    @path = path
    @layout = layout || Spud::Blog.config.base_layout
  end

end
