class SpudPostSite < ActiveRecord::Base
  belongs_to :spud_post

  def spud_site
    return TbCore.site_config_for_id(spud_site_id)
  end

end
