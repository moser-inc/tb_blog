xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{TbCore.site_name} Blog Articles"
    xml.description "Blog articles for #{TbCore.site_name}"
    xml.link posts_path(format: :rss)

    for article in @posts
      xml.item do
        xml.title article.title
        xml.description strip_tags(article.content_processed).truncate(250)
        xml.pubDate article.created_at.to_s(:rfc822)
        xml.link post_path(article.url_name)
        xml.guid article.id
      end
    end
  end
end
