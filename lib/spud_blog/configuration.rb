module Spud
  module Blog
    include ActiveSupport::Configurable
    config_accessor(
      :base_layout, :news_layout, :blogs, :news_enabled, :blog_enabled, :blog_path, :news_path, :posts_per_page,
      :permitted_attributes, :query_for_user, :disqus_shortname
    )
    self.base_layout = 'application'
    self.posts_per_page = 5

    self.news_layout = 'application'
    self.news_enabled = false
    self.blog_enabled = true
    self.blog_path = '/blog'
    self.news_path = '/news'

    self.permitted_attributes = []
    self.query_for_user = nil
    self.disqus_shortname = nil

    self.blogs = []
  end
end
