require 'tb_core'
require 'tb_redirects'
require 'truncate_html'

module Spud
  module Blog
    class Engine < Rails::Engine

      def self.require_controller(controller_name)
        require "#{root}/app/controllers/#{controller_name}"
      end

      engine_name :tb_blog

      initializer :set_up_blogs do
        # Transform config hashes to SpudBlogConfig objects
        Spud::Blog.config.blogs.collect! do |config|
          if config.is_a?(SpudBlogConfig)
            config
          else
            SpudBlogConfig.new(config)
          end
        end
        # This provides support for the legacy :blog_enabled and :news_enabled config options
        # Blog and News handle 90% of the typical use cases
        if Spud::Blog.config.blog_enabled
          SpudBlogConfig.push(key: 'blog',
                              name: 'Blog',
                              path: Spud::Blog.config.blog_path,
                              layout: Spud::Blog.config.base_layout)
        end
        if Spud::Blog.config.news_enabled
          SpudBlogConfig.push(key: 'news',
                              name: 'News',
                              path: Spud::Blog.config.news_path,
                              layout: Spud::Blog.config.news_layout)
        end
      end

      initializer :admin do
        # Build admin modules for each configure blog
        SpudBlogConfig.each do |config|
          blog_app = {
            name: "#{config.name} Posts",
            url: "admin/#{config.key}",
            thumbnail: 'admin/posts_thumb.png'
          }
          TbCore.config.admin_applications += [blog_app]
        end
      end

      initializer 'tb_blog.assets' do
        TbCore.append_admin_javascripts('admin/blog/application')
        TbCore.append_admin_stylesheets('admin/blog/application')
        Rails.application.config.assets.precompile += ['admin/posts_thumb.png', 'admin/news_thumb.png']
      end

      # Triggers a I18n.enforce_available_locales deprecation warning. Why?
      initializer 'tb_blog.user_association', after: 'tb_core.model_overrides' do
        SpudUser.class_eval do
          has_many :posts, class_name: 'SpudPost', dependent: :nullify
        end
      end
    end
  end
end
