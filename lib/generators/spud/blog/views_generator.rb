require 'rails/generators/migration'

class Spud::Blog::ViewsGenerator < ::Rails::Generators::Base

  source_root File.join(Spud::Blog::Engine.root, 'app/views')

  def install
    if Spud::Blog.config.blog_enabled
      copy_file 'posts/_comment_form.html.erb', 'app/views/blog/_comment_form.html.erb'
      copy_file 'posts/index.html.erb', 'app/views/blog/index.html.erb'
      copy_file 'posts/show.html.erb', 'app/views/blog/show.html.erb'
    end
    if Spud::Blog.config.news_enabled
      copy_file 'posts/index.html.erb', 'app/views/news/index.html.erb'
      copy_file 'posts/show.html.erb', 'app/views/news/show.html.erb'
    end
  end
end
