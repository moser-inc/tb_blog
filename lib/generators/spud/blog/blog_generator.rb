class Spud::Blog::BlogGenerator < ::Rails::Generators::Base

  argument :blog_name, type: :string

  source_root File.join(Spud::Blog::Engine.root, 'app/views/posts')

  def generate
    @name = blog_name
    @key = blog_name.parameterize.underscore
    @path = ask('What is your blog path?', default: '/' + @name.parameterize)
    @layout = ask('What layout should your blog use?', default: 'application')
    environment(blog_config())
    if ask('Do you want to use custom views?', limited_to: ['y', 'n']) == 'y'
      copy_file 'index.html.erb', Rails.root.join('app/views', @key, 'index.html.erb')
      copy_file 'show.html.erb', Rails.root.join('app/views', @key, 'show.html.erb')
    end
  end

  private

  def blog_config
    return <<EOF

    Spud::Blog.config.blogs << {
      :key => "#{@key}",
      :name => "#{@name}",
      :path => "#{@path}",
      :layout => "#{@layout}"
    }
EOF
  end

end
