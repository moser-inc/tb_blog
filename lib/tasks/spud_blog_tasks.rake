# desc "Explaining what the task does"
# task :spud_blog do
#   # Task goes here
# end

namespace :tb_blog do

  task apply_blog_keys: :environment do
    SpudPost.all.each do |post|
      if post.is_news?
        post.update_column(:blog_key, 'news')
      else
        post.update_column(:blog_key, 'blog')
      end
    end
  end

end
