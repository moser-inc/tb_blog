$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'spud_blog/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_blog'
  s.version     = Spud::Blog::VERSION
  s.authors     = ['Moser Consulting']
  s.email       = ['greg.woods@moserit.com']
  s.homepage    = 'http://bitbucket.org/moser-inc/tb_blog'
  s.summary     = 'Twice Baked Blog Engine.'
  s.description = 'Twice Baked blogging/news and rss engine.'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'Readme.md']
  s.test_files = Dir.glob('spec/**/*').reject { |f| f.match(%r{^spec\/dummy\/(log|tmp)}) }

  s.add_dependency 'rails', '>= 5.0.0.1'
  s.add_dependency 'tb_core', '>= 1.5.0'
  s.add_dependency 'tb_redirects', '>= 1.0'
  s.add_dependency 'truncate_html'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'pg', '>= 0.18', '< 2.0'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop'
end
